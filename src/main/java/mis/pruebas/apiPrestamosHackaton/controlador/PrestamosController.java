package mis.pruebas.apiPrestamosHackaton.controlador;

import mis.pruebas.apiPrestamosHackaton.models.Cliente;
import mis.pruebas.apiPrestamosHackaton.models.Prestamo;
import mis.pruebas.apiPrestamosHackaton.models.Usuario;
import mis.pruebas.apiPrestamosHackaton.service.PrestamosServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

@CrossOrigin
@RestController
@RequestMapping("api/TechU")
public class PrestamosController {

    private final AtomicLong secuenciaIds = new AtomicLong(0L);

    @Autowired
    PrestamosServicio prestamosServicio;

    @GetMapping("")
    public String root() {
        return "Techu API Prestamos v1.0.0";
    }

    @GetMapping("/clientes/{idCliente}/creditos")
    public List<Prestamo> prestamosPorCliente(@PathVariable String idCliente) {
        Optional<Cliente> cliente = this.prestamosServicio.findById(idCliente);
        if (cliente.isPresent()) {
            return cliente.get().getPrestamos();
        }
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente no encontrado");

    }

    @PostMapping("/clientes")
    public ResponseEntity<Cliente> agregarClientes(@RequestBody Cliente cli) {
        if (validarCURP(cli.getId())) {
            Cliente cliente = this.prestamosServicio.save(cli);
            if (cliente == null)
                throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, "No se agrego el cliente");
            return ResponseEntity.ok(cliente);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, "Formato de CURP incorrecto");
        }
    }

    //Cuenta el número de clientes.
    @GetMapping("/clientesC")
    public long count() {
        return prestamosServicio.count();
    }

    //Listar todos los prestamos.
    @GetMapping("/clientes")
    public List<Cliente> getPrestamos() {
        return prestamosServicio.findAll();
    }

    // PUT
    @PutMapping("/clientes/{id}")
    public void reemplazarCliente(@PathVariable String id, @RequestBody Cliente cliente) {
        final Cliente c = this.prestamosServicio.buscarPorId(id);
        if (c == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        this.prestamosServicio.guardarClientePorId(id, cliente);
    }

    //DELETE
    @DeleteMapping("/clientes/{id}")
    public boolean borrarPorId(@PathVariable String id) {
        Cliente c = prestamosServicio.buscarPorId(id);
        if (c == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        prestamosServicio.eliminarPorCurp(id);
        return true;
    }

    private boolean validarCURP(String curp) {
        String regex =
                "[A-Z]{1}[AEIOU]{1}[A-Z]{2}[0-9]{2}" +
                        "(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])" +
                        "[HM]{1}" +
                        "(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)" +
                        "[B-DF-HJ-NP-TV-Z]{3}" +
                        "[0-9A-Z]{1}[0-9]{1}$";

        Pattern patron = Pattern.compile(regex);
        if (!patron.matcher(curp).matches()) {
            return false;
        } else {
            return true;
        }
    }

    @PostMapping("/clientes/{id}/prestamos")
    public void agregarPrestamoCliente(@PathVariable String id, @RequestBody Prestamo pres) {
        Cliente c = prestamosServicio.buscarPorId(id);
        System.out.println(pres.getMonto());
        if (c == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        if (c.getIngresoMensual() <= 10000) {
            final long id1 = secuenciaIds.incrementAndGet();
            pres.setId(id1);
            List<Prestamo> listaP = new ArrayList<>();
            listaP.add(pres);
            for (Prestamo p : listaP) {
                System.out.println(p.getMonto());
            }
            //System.out.println(listaP.forEach(l->l.getMonto()));
            c.setPrestamos(listaP);
            this.prestamosServicio.guardarClientePorId(id, c);

        }

    }

    // PUT
    @PatchMapping("/clientes/{id}/creditos")
    public boolean agregarCreditoCliente(@PathVariable String id, @RequestBody Prestamo pres) {
        Cliente c = this.prestamosServicio.buscarPorId(id);
        boolean res = false;
        if (c != null) {
            if (c.getIngresoMensual() >= 10000.00 && pres.getMonto() < 50000.00) {
                long idPrestamo = this.secuenciaIds.incrementAndGet();
                pres.setId(idPrestamo);
                c.getPrestamos().add(pres);
                res = this.prestamosServicio.update(c);
            } else {
                throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, "No se puede prestar esa cantidad");
            }
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cliente no encontrado");
        }

        return res;
    }

    @PostMapping("/usuarios")
    public ResponseEntity<Usuario> agregarUsuarios(@RequestBody Usuario u) {
        Usuario usuario = this.prestamosServicio.saveUsuario(u);
        if (usuario == null)
            throw new ResponseStatusException(HttpStatus.NOT_MODIFIED, "No se agrego el usuario");
        return ResponseEntity.ok(usuario);
    }
}