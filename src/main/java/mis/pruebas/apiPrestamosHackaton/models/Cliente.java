package mis.pruebas.apiPrestamosHackaton.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "cliente")
public class Cliente {
    @Id
    private String id;
    private String nombre;
    private String apellidoMaterno;
    private String apellidoPaterno;
    private String fecha;
    private String genero;
    private String estado;
    private String celular;
    private Double ingresoMensual;
    private List<FormaDePago> formaDePago;
    private Direccion direccion;
    private List<Prestamo> prestamos;

    public Cliente(String id, String nombre, String apellidoMaterno, String apellidoPaterno, String fecha, String genero, String estado, String celular, Double ingresoMensual, List<FormaDePago> formaDePago, Direccion direccion, List<Prestamo> prestamos) {
        this.id = id;
        this.nombre = nombre;
        this.apellidoMaterno = apellidoMaterno;
        this.apellidoPaterno = apellidoPaterno;
        this.fecha = fecha;
        this.genero = genero;
        this.estado = estado;
        this.celular = celular;
        this.ingresoMensual = ingresoMensual;
        this.formaDePago = formaDePago;
        this.direccion = direccion;
        this.prestamos = prestamos;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidoMaterno() {
        return apellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        this.apellidoMaterno = apellidoMaterno;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Double getIngresoMensual() {
        return ingresoMensual;
    }

    public void setIngresoMensual(Double ingresoMensual) {
        this.ingresoMensual = ingresoMensual;
    }

    public List<FormaDePago> getFormaDePago() {
        if (this.formaDePago == null) {
            this.formaDePago = new ArrayList<>();
        }
        return formaDePago;
    }

    public void setFormaDePago(List<FormaDePago> formaDePago) {
        this.formaDePago = formaDePago;
    }

    public Direccion getDireccion() {
        if (this.direccion == null) {
            this.direccion = new Direccion();
        }
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

    public List<Prestamo> getPrestamos() {
        if(this.prestamos == null){
            this.prestamos = new ArrayList<>();
        }
        return prestamos;
    }

    public void setPrestamos(List<Prestamo> prestamos) {
        this.prestamos = prestamos;
    }


}
