package mis.pruebas.apiPrestamosHackaton.models;

public class Prestamo {
    private long id;
    private double monto;
    private String fechaApertura;

    public Prestamo() {
    }

    public Prestamo(long id, Double monto, String fechaApertura) {
        this.id = id;
        this.monto = monto;
        this.fechaApertura = fechaApertura;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public String getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(String fechaApertura) {
        this.fechaApertura = fechaApertura;
    }
}
