package mis.pruebas.apiPrestamosHackaton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPrestamosHackatonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPrestamosHackatonApplication.class, args);
	}

}
