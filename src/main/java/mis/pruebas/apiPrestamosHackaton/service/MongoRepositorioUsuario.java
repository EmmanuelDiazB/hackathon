package mis.pruebas.apiPrestamosHackaton.service;

import mis.pruebas.apiPrestamosHackaton.models.Cliente;
import mis.pruebas.apiPrestamosHackaton.models.Usuario;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoRepositorioUsuario extends MongoRepository<Usuario, String> {

}
