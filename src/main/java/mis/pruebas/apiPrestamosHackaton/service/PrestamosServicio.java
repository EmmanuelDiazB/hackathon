package mis.pruebas.apiPrestamosHackaton.service;

import mis.pruebas.apiPrestamosHackaton.models.Cliente;
import mis.pruebas.apiPrestamosHackaton.models.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PrestamosServicio {


    @Autowired
    MongoRepositorio mongoRepository;

    @Autowired
    MongoRepositorioUsuario mongoRepositoryUsuario;

    public Optional<Cliente> findById(String id) {
        return this.mongoRepository.findById(id);
    }

    public Cliente save(Cliente cli) {
        return this.mongoRepository.insert(cli);
    }

    //Cuenta el número de clientes.
    public long count() {
        return mongoRepository.count();
    }

    public List<Cliente> findAll() {
        return mongoRepository.findAll();
    }

    public Usuario saveUsuario(Usuario u) {
        return this.mongoRepositoryUsuario.insert(u);
    }
    public boolean update(Cliente c) {
        try {
            this.mongoRepository.save(c);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Cliente buscarPorId(String id) {
        Optional<Cliente> cliente = this.mongoRepository.findById(id);
        return cliente.isPresent() ? cliente.get() : null;
    }

    public void guardarClientePorId(String idCliente, Cliente cliente) {
        cliente.setId(idCliente);
        this.mongoRepository.save(cliente);
    }

    @Autowired
    MongoOperations mongoOperations;

    public void eliminarPorCurp(String Curp) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("id").is(Curp));
        Cliente cliente = mongoOperations.findAndRemove(query, Cliente.class);
        System.out.println("Deleted document : " + cliente);
    }

}
