package mis.pruebas.apiPrestamosHackaton.service;

public interface RepositorioPersonalizado {
    public boolean eliminarPorCurp(String Curp);
    public boolean findByCurp(String Curp);
}

