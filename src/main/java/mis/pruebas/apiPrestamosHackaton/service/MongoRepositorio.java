package mis.pruebas.apiPrestamosHackaton.service;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import mis.pruebas.apiPrestamosHackaton.models.*;

@Repository
public interface MongoRepositorio extends MongoRepository<Cliente, String> {

}
