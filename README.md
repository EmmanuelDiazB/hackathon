﻿## Hackaton 


**URL**: localhost:8888/api/TechU/

## Métodos HTTP 

|                Descripción|Método|URL|
|----------------|-------------------------------|-----------------------------|
|Añadir un nuevo cliente(**Eli**)          |POST            |/clientes
|Mostrar todos los usuarios(**Dany**)|GET      |/clientes|
|Listar los créditos un cliente(**Eli**)|GET            |/clientes/{id}/creditos|
|Añadir un préstamo a un cliente(**EED**)          |POST            |/clientes/{id}/creditos
|Contar el número de clientes(**Dany**)|GET      |/clientesC|
|Actualizar información de un usuario(**Emma**)|PUT|/clientes/{id}|
|Eliminar información de un usuario(**Emma**)|DELETE|/clientes/{id}|
|Agregar un crédito a un cliente(**EED**)|PATCH|/clientes/{id}/creditos|
|Agregar un nuevo usuario(**EED**)|POST|/usuarios|




**JSON**

{
	"id_curp": "",
	"nombre": "",
	"apellidoMaterno": "",
	"apellidoPaterno": "",
	"date": "",
	"genero": "",
	"estado": "",
	"celular": "",
	"ingresoMensual": "",
	"formaDePago":
	{
		"banco": "",
		"cuenta": ""
	},

direccion: 
{
	"calle": "",
	"numeroExterior": "",
	"numeroInterior": "", 
	"colonia": "",
	"municipio": "",
	"estado": "",
	"codigoPostal": ""
},
prestamos:
{
	"id": "",
	"monto": "",
	"fechaDeApertura": ""
}

 
Usuario
{
	"email": "",
	"contrasena": ""
}	
	


